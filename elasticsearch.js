const {Client} = require('@elastic/elasticsearch')
const process = require("process");
const {hostname} = require("os");
const {readFileSync} = require("fs");
require('dotenv').config();

const client = new Client({
    node: process.env.ES_HOST,
    auth: {
        apiKey: {
            id: process.env.ES_API_KEY_ID,
            api_key: process.env.ES_API_KEY
        }
    },
    ssl: {
        ca: readFileSync(process.env.ES_CA_CERT),
        rejectUnauthorized: true
    }
})

const INTERVAL_MINUTES = 1;

/**
 * Track alerts from elasticsearch and send them to the callback
 * @param callback - function to call with the alert 'IMessage object'
 */
function trackAlerts(callback = console.log) {
    callback(getHelloMessage());


    setInterval(function doSearch() {
        console.log("Checking for alerts");
        client.search({
            index: '.alerts-security*',
            body: {
                query: {
                    bool: {
                        filter: [
                            {
                                range: {
                                    '@timestamp': {
                                        gte: `now-${INTERVAL_MINUTES}m`,
                                        lte: 'now'
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }).then(({body}) => {
                if (body.hits.total.value > 0) {
                    body.hits.hits.forEach((hit) => {
                        console.log(hit, hit._source['kibana.alert.threshold_result']?.terms);
                        var alertName = hit._source['kibana.alert.rule.name'];
                        var tagsStr = '`' + hit._source['kibana.alert.rule.tags'].join('` `') + '`';
                        var emoji = hit._source['kibana.alert.risk_score'] > 80 ? ':fire:' : ':warning:';

                        // Build the command line if exists
                        var parentProcess = hit._source.process?.parent ? hit._source.process.parent.executable : '';
                        var commandLine = hit._source.process ? `${parentProcess ? `\`${parentProcess}\` ` : ''}\`${hit._source.process.command_line}\`` : null;

                        var attachments = [];
                        if (commandLine) {
                            attachments.push({
                                title: `Commande`,
                                value: commandLine,
                                short: false
                            })
                        }


                        hit._source['kibana.alert.threshold_result']?.terms?.forEach((term) => {
                            attachments.push({
                                title: term.key,
                                value: term.value,
                                short: true
                            })
                        });

                        // Add client info if exists
                        if (hit._source.client) {
                            attachments.push({
                                title: `Client`,
                                value: `\`${hit._source.client.ip}\` - ${hit._source.client.geo?.country_name} ${hit._source.client.geo?.city_name ? "(" + hit._source.client.geo.city_name + ')' : ''}`,
                                short: true
                            })
                        }

                        if (hit._source.threat && hit._source.threat.enrichments.indicator && hit._source.threat.enrichments.indicator.description) {
                            attachments.push({
                                title: `Indicator desc.`,
                                value: hit._source.threat.enrichments.indicator.description,
                                short: true
                            })
                        }

                        if (hit._source.dns && hit._source.dns.record) {
                            attachments.push({
                                title: `DNS record`,
                                value: hit._source.dns.record,
                                short: true
                            })

                            
                            if (hit._source.dns.client_addr) {
                                attachments.push({
                                    title: `Client DNS`,
                                    value: hit._source.dns.client_addr,
                                    short: true
                                })
                            }
                        }



                        callback({
                            alias: "Elastic Bot",
                            emoji: emoji,
                            blocks: [
                                {
                                    type: "section",
                                    text: {
                                        type: "mrkdwn",
                                        text: `**${alertName}**\n${tagsStr}`
                                    }
                                },
                                {
                                    type: 'context',
                                    block_id: 'kibana.alert.original_time',
                                    elements: [
                                        {
                                            type: 'mrkdwn',
                                            text: "**heure**"
                                        },
                                        {
                                            type: 'plain_text',
                                            text: hit._source['kibana.alert.original_time']
                                        }
                                    ]
                                },
                                {
                                    type: 'context',
                                    blockId: 'kibana.alert.reason',
                                    elements: [
                                        {
                                            type: 'mrkdwn',
                                            text: `**raison** : ${hit._source['kibana.alert.reason']}\n**description** : ${hit._source['kibana.alert.rule.description']}`
                                        }
                                    ]
                                },
                                {
                                    type: "section",
                                    text: {
                                        type: "mrkdwn",
                                        text: `**Détails**\n${attachments.map(a => ` - ${a.title ? a.title + " : " : ""} ${a.value}`).join('\n')}`
                                    }
                                }
                            ],
                            // attachments: [
                            //     {
                            //         title: `Détails`,
                            //         collapsed: false,
                            //         color: hit._source['kibana.alert.risk_score'] > 80 ? 'red' : hit._source['kibana.alert.risk_score'] > 50 ? 'orange' : 'yellow',
                            //         fields: attachments
                            //     }]
                        });

                    });
                }
            }
        ).catch((error) => {
            console.log("Error getting response from elasticsearch");
            console.log(error);
            error = error.meta.body
            callback({
                msg: `\`${hostname()}\`  Error getting response from elasticsearch`,
                attachments: [
                    {
                        title: `${error.status} - ${error.error.type}`,
                        collapsed: true,
                        text: `${error.error.reason}`,
                        color: 'red'
                    }
                ]
            });
        });

        return doSearch;
    }(), INTERVAL_MINUTES * 60 * 1000
    )
    ;
}

function getHelloMessage() {
    return {
        emoji: ':information_source:',
        alias: "Elastic Bot",
        blocks: [
            {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: `\`${hostname()}\` suit désormais les alertes de sécurité`
                }
            },
            {
                type: 'context',
                blockId: 'context_1',
                elements: [
                    {
                        type: 'plain_text',
                        text: `Actuellement, je suis configuré pour suivre les alertes de sécurité dans les index \`.alerts-security*\` toutes les ${INTERVAL_MINUTES} minutes.`
                    }
                ]
            }
        ]
    }
}

module.exports = {
    trackAlerts
}
