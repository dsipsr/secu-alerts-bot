# Bot elasticsearch -> teamchat

L'intégration Slack/Teamchat de Elasticsearch pour la retransmission des alertes est payante :'(

Ce "bot" permet juste d'aller lire à intervalles réguliers (toutes les minutes) les alertes de sécurité générées dans
ElasticSearch.

L'intervalle de check est modifiable depuis le fichier `elasticsearch.js`, constante `INTERVAL_MINUTES`.

## Installation

```bash
apt install npm
npm install
```

## Configuration

Toutes les variables sont à renseigner dans le .env (copier le .env.example et le renommer en .env).

Il est également possible d'utiliser l'environnement d'éxécution pour passer les variables d'environnement.

## Utilisation

```bash
npm start
```

## Déploiement en tant que service

Editer le fichier `elasticsearch-bot.service` et modifier le chemin d'accès au fichier `elasticsearch.js`.

Vérifier que l'utilisateur qui va lancer le service a bien les droits sur le fichier.

```bash
cp elasticsearch-bot.service /etc/systemd/system/elasticsearch-bot.service
systemctl daemon-reload
systemctl enable elasticsearch-bot.service
systemctl start elasticsearch-bot.service
```

