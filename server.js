const {driver} = require('@rocket.chat/sdk');
const respmap = require('./reply');
const {trackAlerts} = require("./elasticsearch");
const {hostname} = require("os");

require('dotenv').config();

// Environment Setup
const HOST = process.env.TEAMCHAT_HOST;
const USER = process.env.TEAMCHAT_USER;
const PASS = process.env.TEAMCHAT_PASS;
const SSL = process.env.TEAMCHAT_SSL;
const ROOMS = process.env.TEAMCHAT_ROOMS.split(',');

var myUserId;

// Bot configuration
const runbot = async () => {
    await driver.connect({host: HOST, useSsl: SSL})
    myUserId = await driver.login({username: USER, password: PASS});

    console.log('connected ' + myUserId);

    console.log("Try to join rooms", ROOMS);
    await driver.joinRooms(ROOMS);
    console.log('joined rooms');

    await driver.subscribeToMessages();
    console.log('subscribed');

    await driver.reactToMessages(processMessages);
    console.log('connected and waiting for messages');

    const rid = await driver.getRoomId(ROOMS[0]);
    await trackAlerts((messageObject) => {
        var message = driver.prepareMessage(messageObject, rid)
        driver.sendMessage(message);
    });
}

// Process messages
const processMessages = async (err, message) => {
    if (!err) {
        if (message.u._id === myUserId) return;
        const roomname = await driver.getRoomName(message.rid);

        console.log(`got message ${message.msg} in room ${roomname}`)
        msg = message.msg.toLowerCase();

        let response;
        // Get first matching response
        for (const key in respmap) {
            if (msg.includes(key.toLowerCase())) {
                response = respmap[key];
                await driver.sendToRoomId(response, message.rid)
                break;
            }
        }
    }
}


runbot().catch((error) => {
    console.error("BOT failed:", error);
    driver.getRoomId(ROOMS[0]).then((rid) => {
        var message = driver.prepareMessage(
            {
                emoji: ':warning:',
                msg: `\`${hostname()}\`  **BOT tout cassé**`,
                attachments: [
                    {
                        color: '#FF0000',
                        title: 'Erreur détaillée',
                        collapsed: true,
                        text: `\`\`\`bash\n${error}\n\`\`\``
                    }
                ]
            }, rid)
        // var message = driver.prepareMessage({
        //     msg: `**BOT failed:**\n \`\`\`\n${error}\n\`\`\` on \`${hostname()}\``,
        // }, rid)
        driver.sendMessage(message).then(() => process.exit(1));
    });
});
